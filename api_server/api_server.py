import logging

from flask import render_template
from flask_classy import FlaskView, route, request

from manager import Manager
from models.controller import Controller

logger = logging.getLogger(__name__)


class ApiServer(FlaskView):

    @route("/", methods=["GET"])
    def index(self):
        return render_template("index.html", controllers=Manager.get_controllers())

    @route("/controller/register", methods=["GET", "POST"])
    def register_controller(self):
        controller_name = request.values["controller"]

        controller = Manager.get_controller(controller_name)
        if controller is None:
            logger.info("Registering New Controller: " + controller_name)
        else:
            logger.info("Updating Controller: " + controller_name)

        json_data = dict()
        json_data["name"] = controller_name
        json_data["address"] = "http://" + request.remote_addr

        Controller.from_json(json_data).save()
        Manager.update_controller()

        return ""

    @route("/led/get_modes", methods=["GET", "POST"])
    def get_modes(self):
        controller_name = request.values["controller"]

        controller = Manager.get_controller(controller_name)

        if controller is None:
            return "Controller %s not found!" % controller_name, 400

        return render_template('led_modes.html', controller_name=controller_name, led_modes=controller.get_modes())

    @route("/led/set_mode", methods=["GET", "POST"])
    def set_mode(self):
        controller_name = request.values["controller"]
        mode = request.values["mode"]

        controller = Manager.get_controller(controller_name)
        led_mode = controller.get_mode(mode)

        # Got all params?
        post_params = dict()
        for p in led_mode.params:
            p = p.name
            if p not in request.values:
                return render_template('set_led_mode.html', controller_name=controller_name, led_mode=led_mode)
            else:
                post_params[p] = request.values[p]

        # Yes -> apply
        if controller.set_mode(led_mode.name, post_params):
            return render_template('mode_successfully_set.html', controller_name=controller_name, led_mode=led_mode)
        else:
            return "Communication error with controller!", 500
