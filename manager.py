import json
import logging
from os import listdir
from os.path import join

from models.controller import Controller
from settings import Settings

logger = logging.getLogger(__name__)


class Manager:

    controller = []

    @staticmethod
    def init():
        Manager.update_controller()

    @staticmethod
    def get_controllers():
        return Manager.controller

    @staticmethod
    def get_controller(name):
        for c in Manager.controller:
            if c.name == name:
                return c
        return None

    @staticmethod
    def update_controller():
        Manager.controller.clear()
        c_files = listdir(Settings.controller_dir)
        for c_f in c_files:
            if c_f.endswith(".json"):
                with open(join(Settings.controller_dir, c_f), "r") as f:
                    data = json.loads(f.read())

                    logger.info("Loaded Controller: %s [%s]" % (data["name"], data["address"]))

                    Manager.controller.append(Controller.from_json(data))
