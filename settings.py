import logging

from os.path import abspath, join


logger = logging.getLogger(__name__)


class Settings:

    # All dirs are made to absolute paths in init()
    base_dir = "."
    room_dir = "Rooms"
    controller_dir = "Controller"

    @staticmethod
    def init():
        Settings.base_dir = abspath(Settings.base_dir)
        logger.info("Run server in directory %s", Settings.base_dir)
        Settings.room_dir = join(Settings.base_dir, Settings.room_dir)
        Settings.controller_dir = join(Settings.base_dir, Settings.controller_dir)
