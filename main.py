import coloredlogs
from flask import Flask

from api_server.api_server import ApiServer
from manager import Manager
from settings import Settings

# Specify log output format
LOGFORMAT = "%(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"


if __name__ == "__main__":
    app = Flask(__name__)

    # Init log
    coloredlogs.install()

    # Init Settings
    Settings.init()

    # Init Manager
    Manager.init()

    ApiServer.register(app, route_base="/")
    app.run(host="0.0.0.0", port=1337)
