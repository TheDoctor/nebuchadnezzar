import json
import logging
from os.path import join

import requests

from models.led_mode import LEDMode
from settings import Settings

logger = logging.getLogger(__name__)


class Controller:

    def __init__(self, name, address):
        self.name = name
        self.address = address

        # Ask controller
        resp = requests.post(self.address + "/led/get_supported_modes")
        json_data = resp.json()

        # Parse LED methods
        self.led_modes = []
        for mode in json_data:
            self.led_modes.append(LEDMode.from_json(mode))

        # TODO: Remove Debug
        #self.led_modes = [LEDMode("SingleColor", "Catchy description", "r&g&b"),
        #                  LEDMode("Rainbow", "Somewhere over the Rainbow", "")]

    @classmethod
    def from_json(cls, json_data):
        return cls(json_data["name"], json_data["address"])

    def to_json(self):
        json_data = dict()
        json_data["name"] = self.name
        json_data["address"] = self.address

        return json_data

    def set_mode(self, name, params):
        params["name"] = name
        resp = requests.post(self.address + "/led/set_mode", data=params)

        return resp.ok

    def get_modes(self):
        return self.led_modes

    def get_mode(self, name):
        for m in self.led_modes:
            if m.name == name:
                return m

        return None

    def save(self):
        with open(join(Settings.controller_dir, self.name + ".json"), "w") as f:
            f.write(json.dumps(self.to_json()))

        logger.info("Saved %s" % self.name)
