import json
from os.path import isabs, join

from settings import Settings


class Room:

    def __init__(self, room_file):
        if not isabs(room_file):
            room_file = join(Settings.room_dir, room_file)

        data = None
        with open(room_file, "r") as f:
            data = json.loads(f.read())

            # Room name
            self.name = data["name"]

        # TODO: Continue

    def to_json(self):
        pass