from models.led_param import LEDParam


class LEDMode:

    def __init__(self, name, description, params):
        self.name = name
        self.description = description
        self.params = self.parse_params(params.split("&")) if len(params) > 0 else []

    def parse_params(self, params):
        return [LEDParam(p) for p in params]

    @classmethod
    def from_json(cls, json_data):
        return cls(json_data["name"], json_data["description"], json_data["params"])
