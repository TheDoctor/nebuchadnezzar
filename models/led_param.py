

class LEDParam:

    def __init__(self, param):
        # Param type
        splitted = param.split(":")
        self.type = splitted[0]

        # Param name
        self.name = splitted[2]

        # Param min/max
        minMax = splitted[1].split("-")
        self.min = minMax[0]
        self.max = minMax[1]
